<?php

namespace Drupal\Tests\link_tracker\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\link_tracker\FieldExtractorTrait;

/**
 * Test ConvertUriToAbsoluteUri.
 *
 * @group link_tracker
 */
class ConvertUriToAbsoluteUriTest extends EntityKernelTestBase {

  use FieldExtractorTrait;

  /**
   * The modules required to run the test.
   *
   * @var array
   */
  public static $modules = [
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/node/1')
      ->save();
  }

  /**
   * Test ConvertUriToAbsoluteUri.
   *
   * @param string $uri
   *   The uri input.
   * @param string $absolute_uri
   *   Absolute uri.
   *
   * @dataProvider dataProvider
   */
  public function testConvertToAbsoluteLink($uri, $absolute_uri) {
    $this->assertEquals($absolute_uri, $this->convertUriToAbsoluteUri($uri), "Links not converted correctly");
  }

  /**
   * Data provider for testConvertToAbsoluteLink.
   *
   * Note that $base_url always resolves to http://localhost in
   * \Drupal\KernelTests\KernelTestBase
   *
   * @return array
   *   The input and expected output.
   */
  public function dataProvider() {
    return [
      ['http://example.com', 'http://example.com'],
      ['http://example.com/link-1', 'http://example.com/link-1'],
      ['internal:/', 'http://localhost/'],
      ['internal:/node/1', 'http://localhost/node/1'],
      ['base:robots.txt', 'http://localhost/robots.txt'],
      ['entity:user/1', 'http://localhost/user/1'],
      ['route:system.401', 'http://localhost/system/401'],
      ['/', 'http://localhost/'],
      ['?q=1', '?q=1'],
      ['#step-1', '#step-1'],
      ['/node/1', 'http://localhost/node/1'],
      ['/node/1#step-1', 'http://localhost/node/1#step-1'],
      ['<front>', 'http://localhost/'],
      ['<none>', ''],
    ];
  }

}
