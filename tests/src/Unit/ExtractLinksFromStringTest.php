<?php

namespace Drupal\Tests\link_tracker\Unit;

use Drupal\link_tracker\FieldExtractorTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Test ExtractLinksFromString.
 *
 * @group link_tracker
 */
class ExtractLinksFromStringTest extends UnitTestCase {

  use FieldExtractorTrait;

  /**
   * Test ExtractLinksFromString.
   *
   * @param string $string
   *   The string to extract links from.
   * @param [] $found_links
   *   Array with found links.
   *
   * @dataProvider dataProvider
   */
  public function testExtractLinksFromString($string, $found_links) {
    $this->assertEquals($found_links, $this->extractLinksFromString($string), "Links not found.");
  }

  /**
   * Data provider for testExtractLinksFromString.
   *
   * @return array
   *   The string input and expected array of links output.
   */
  public function dataProvider() {
    return [
      ['no url here', []],
      ['http://example.com', []],
      ['http://example.com/link-1 ... http://example.com/link-2', []],
      ['<p><a href="http://example.com/link-1">Link 1</a> ... <a href="http://example.com/link-2">Link 2</a></p>', ['http://example.com/link-1', 'http://example.com/link-2']],
      ["<p><a href='http://example.com/link-1'>Link 1</a> ... <a href='http://example.com/link-2'>Link 2</a></p>", ['http://example.com/link-1', 'http://example.com/link-2']],
    ];
  }

}
