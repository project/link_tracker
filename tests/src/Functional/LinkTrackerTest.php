<?php

namespace Drupal\Tests\link_tracker\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\link\LinkItemInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for link_tracker module.
 *
 * @group link_tracker
 */
class LinkTrackerTest extends BrowserTestBase {

  /**
   * The modules required to run the test.
   *
   * @var array
   */
  public static $modules = [
    'entity_track',
    'entity_track_test',
    'field_ui',
    'filter',
    'link',
    'link_tracker',
    'node',
    'path',
    'system',
    'text',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * A node-object used in this test class.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * A node-data-structure used in this test class.
   *
   * @var []
   */
  protected $node_data;

  /**
   * Returns a node structure with randomized test data.
   *
   * @return array
   */
  private function getExampleNodeData() {
    return $this->node_data ?? $this->node_data = [
      'type' => 'entity_test',
      'title' => md5(mt_rand()),
      'field_text' => [
        'value' => '<p>Text with two links <a href="http://example.com/field_text-link-1">Link 1</a> and <a href="http://example.com/field_text-link-2">Link 2</a></p>',
      ],
      'field_text_unlimited' => [
        [
          'value' => '<p>Text with two links <a href="http://example.com/field_text_unlimited-link-1">Link 1</a> and <a href="http://example.com/field_text_unlimited-link-2">Link 2</a></p>',
        ],
        [
          'value' => '<p>Text with two links <a href="http://example.com/field_text_unlimited-link-3">Link 3</a> and <a href="http://example.com/field_text_unlimited-link-4">Link 4</a></p>',
        ],
      ],
      'field_text_long' => [
        'value' => '<p>Text with two links <a href="http://example.com/field_text_long-link-1">Link 1</a> and <a href="http://example.com/field_text_long-link-2">Link 2</a></p>',
      ],
      'field_text_with_summary' => [
        'value' => '<p>Text with two links <a href="http://example.com/field_text_with_summary-link-1">Link 1</a> and <a href="http://example.com/field_text_with_summary-link-2">Link 2</a></p>',
        'summary' => 'The summary is plain text in Drupal core and we **ignore** it for link tracking',
      ],
      'field_link_single' => [
        [
          'uri' => 'http://example.com/field_link_single-link-1',
          'title' => 'Link 1',
        ],
      ],
      'field_link_unlimited' => [
        [
          'uri' => 'http://example.com/field_link_unlimited-link-1',
          'title' => 'Link 1',
        ],
        [
          'uri' => 'http://example.com/field_link_unlimited-link-2',
          'title' => 'Link 2',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Enable plugins.
    $config = \Drupal::configFactory()->getEditable('entity_track.settings');
    $plugins = ['link_tracker_link_field', 'link_tracker_link_from_text'];
    $config->set('track_enabled_plugins', $plugins)->save(TRUE);

    // Create a node with fields that contain link-data.
    $this->drupalCreateContentType([
      'type' => 'entity_test',
    ])->save();

    // Create a text field.
    FieldStorageConfig::create([
      'field_name' => 'field_text',
      'entity_type' => 'node',
      'type' => 'text',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_text',
      'bundle' => 'entity_test',
    ])->save();

    // Create a multi-value text field.
    FieldStorageConfig::create([
      'field_name' => 'field_text_unlimited',
      'entity_type' => 'node',
      'type' => 'text',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_text_unlimited',
      'bundle' => 'entity_test',
    ])->save();

    // Create a text-long field.
    FieldStorageConfig::create([
      'field_name' => 'field_text_long',
      'entity_type' => 'node',
      'type' => 'text_long',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_text_long',
      'bundle' => 'entity_test',
    ])->save();

    // Create a text-long field.
    FieldStorageConfig::create([
      'field_name' => 'field_text_with_summary',
      'entity_type' => 'node',
      'type' => 'text_with_summary',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_text_with_summary',
      'bundle' => 'entity_test',
    ])->save();

    // Create a single-value link field.
    FieldStorageConfig::create([
      'field_name' => 'field_link_single',
      'entity_type' => 'node',
      'type' => 'link',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_link_single',
      'bundle' => 'entity_test',
      'settings' => ['link_type' => LinkItemInterface::LINK_GENERIC],
    ])->save();

    // Create a multi-value link field.
    FieldStorageConfig::create([
      'field_name' => 'field_link_unlimited',
      'entity_type' => 'node',
      'type' => 'link',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_link_unlimited',
      'bundle' => 'entity_test',
      'settings' => ['link_type' => LinkItemInterface::LINK_GENERIC],
    ])->save();


    // Add string with multiple links.
    // Add text field with multiple links.
    // Add text_with_summary field with multiple links
    // Add multivalue link-field
    // Add singlevalue link-field
    $node = $this->drupalCreateNode($this->getExampleNodeData());
    $node->save();
    $this->node = $node;
  }

  /**
   * Test content creation.
   */
  public function testContentCreation() {
    // Text field.
    self::assertStringContainsString('http://example.com/field_text-link-1', $this->node->get('field_text')->getString());
    self::assertStringContainsString('http://example.com/field_text-link-2', $this->node->get('field_text')->getString());
    // Text unlimited field.
    self::assertStringContainsString('http://example.com/field_text_unlimited-link-1', $this->node->get('field_text_unlimited')->getString());
    self::assertStringContainsString('http://example.com/field_text_unlimited-link-2', $this->node->get('field_text_unlimited')->getString());
    self::assertStringContainsString('http://example.com/field_text_unlimited-link-3', $this->node->get('field_text_unlimited')->getString());
    self::assertStringContainsString('http://example.com/field_text_unlimited-link-4', $this->node->get('field_text_unlimited')->getString());
    // Text long field.
    self::assertStringContainsString('http://example.com/field_text_long-link-1', $this->node->get('field_text_long')->getString());
    self::assertStringContainsString('http://example.com/field_text_long-link-2', $this->node->get('field_text_long')->getString());
    // Text with summary field.
    self::assertStringContainsString('http://example.com/field_text_with_summary-link-1', $this->node->get('field_text_with_summary')->getString());
    self::assertStringContainsString('http://example.com/field_text_with_summary-link-2', $this->node->get('field_text_with_summary')->getString());
    // Link field single-value.
    self::assertEquals('http://example.com/field_link_single-link-1', $this->node->get('field_link_single')
      ->get(0)
      ->getValue()['uri']);
    // Link field multi-value.
    self::assertEquals('http://example.com/field_link_unlimited-link-1', $this->node->get('field_link_unlimited')
      ->get(0)
      ->getValue()['uri']);
    self::assertEquals('http://example.com/field_link_unlimited-link-2', $this->node->get('field_link_unlimited')
      ->get(1)
      ->getValue()['uri']);
  }

  /**
   * Test TrackedLinkManager with CRUD operations.
   */
  public function testTrackedLinkManager() {
    /* @var \Drupal\link_tracker\TrackedLinkManager $tracked_link_manager */
    $tracked_link_manager = \Drupal::service('link_tracker.tracked_link_manager');

    /* @var \Drupal\Core\Entity\EntityStorageInterface $tracked_link_storage */
    $tracked_link_storage = \Drupal::service('entity_type.manager')->getStorage('tracked_link');

    // In setUp() a test-node has been created.
    self::assertCount(2, $tracked_link_manager->loadByEntityAndField($this->node, 'field_text'));
    self::assertCount(4, $tracked_link_manager->loadByEntityAndField($this->node, 'field_text_unlimited'));
    self::assertCount(2, $tracked_link_manager->loadByEntityAndField($this->node, 'field_text_long'));
    self::assertCount(2, $tracked_link_manager->loadByEntityAndField($this->node, 'field_text_with_summary'));
    self::assertCount(1, $tracked_link_manager->loadByEntityAndField($this->node, 'field_link_single'));
    self::assertCount(2, $tracked_link_manager->loadByEntityAndField($this->node, 'field_link_unlimited'));
    self::assertCount(13, $tracked_link_manager->loadByEntity($this->node));
    self::assertCount(13, $tracked_link_storage->getQuery()->execute());

    // Update field-data (adding 1 additional url that must be tracked).
    $this->node->get('field_text_long')->setValue([
      'value' => '<p>Updated text field with <strong>three</strong> links <a href="http://example.com/field_text-link-updated-1">Link 1 updated</a> and <a href="http://example.com/field_text-link-updated-2">Link 2 updated</a> and <a href="http://example.com/field_text-link-updated-3">Link 3 updated</a></p>',
    ]);
    $this->node->save();
    self::assertCount(3, $tracked_link_manager->loadByEntityAndField($this->node, 'field_text_long'));
    self::assertCount(14, $tracked_link_manager->loadByEntity($this->node));
    self::assertCount(14, $tracked_link_storage->getQuery()->execute());

    // Delete data.
    $this->node->delete();
    self::assertCount(0, $tracked_link_storage->getQuery()->execute());
  }

  /**
   * Test TrackedLinkManager loadByLastChecked().
   */
  public function testTrackedLinkManagerLoadByLastChecked() {
    /* @var \Drupal\link_tracker\TrackedLinkManager $tracked_link_manager */
    $tracked_link_manager = \Drupal::service('link_tracker.tracked_link_manager');

    // Initial loadByLastChecked() return only items that have not been checked.
    self::assertCount(13, $tracked_link_manager->loadByLastChecked());
    // Limit functionality is optional.
    self::assertCount(12, $tracked_link_manager->loadByLastChecked(12));

    // Now we will add timestamps to tracked links.
    // Note that we use low timestamp numbers for readability.
    foreach ($tracked_link_manager->loadByLastChecked(4) as $link) {
      $link->set('link_last_checked', 22222)->save();
    }
    self::assertCount(9, $tracked_link_manager->loadByLastChecked());
    foreach ($tracked_link_manager->loadByLastChecked(4) as $link) {
      $link->set('link_last_checked', 33333)->save();
    }
    self::assertCount(5, $tracked_link_manager->loadByLastChecked());
    foreach ($tracked_link_manager->loadByLastChecked() as $link) {
      $link->set('link_last_checked', 44444)->save();
    }

    // When all links have a timestamp, all must be returned.
    self::assertCount(13, $tracked_link_manager->loadByLastChecked());
    // And now we are able to test the timestamp filter.
    // 4 items have been checked at 22222 and 0 earlier.
    self::assertCount(4, $tracked_link_manager->loadByLastChecked(50, 22223));
    self::assertCount(0, $tracked_link_manager->loadByLastChecked(50, 22222));
    self::assertCount(0, $tracked_link_manager->loadByLastChecked(50, 22221));

    // 4 items have been checked at 33333 and 4 earlier.
    self::assertCount(8, $tracked_link_manager->loadByLastChecked(50, 33334));
    self::assertCount(4, $tracked_link_manager->loadByLastChecked(50, 33333));
    self::assertCount(4, $tracked_link_manager->loadByLastChecked(50, 33332));

    // 5 items have been checked at 44444 and 8 earlier.
    self::assertCount(13, $tracked_link_manager->loadByLastChecked(50, 44445));
    self::assertCount(8, $tracked_link_manager->loadByLastChecked(50, 44444));
    self::assertCount(8, $tracked_link_manager->loadByLastChecked(50, 44443));
  }

}
