<?php

namespace Drupal\link_tracker;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Trait FieldExtractorTrait
 *
 * @package Drupal\link_tracker
 */
trait FieldExtractorTrait {

  /**
   * Returns field-values for text-fields.
   *
   * @param $entity
   *  Entity to extract field values from.
   *
   * @return array
   *   Flatten array with field values.
   */
  protected function extractFieldValues(EntityInterface $entity) {
    $extracted_field_values = [];

    $fields = array_keys($this->getReferencingFields($entity, $this->getApplicableFieldTypes()));
    foreach ($fields as $field_name) {
      if ($entity->hasField($field_name) && !$entity->{$field_name}->isEmpty()) {
        /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
        foreach ($entity->{$field_name} as $field_item) {
          $extracted_field_values[$field_name][] = $field_item->getValue();
        }
      }
    }

    return $extracted_field_values;
  }

  /**
   * Extract URI's from text through the href-attribute.
   *
   * @param string $string
   *   Input text that can contain one or more URI's.
   *
   * @return array
   *   Flatten array of URI's found in input.
   */
  private function extractLinksFromString(string $string) {
    $match = [];

    $dom = Html::load($string);
    $xpath = new \DOMXPath($dom);
    $xpath_query = "//a[@href != '']";
    foreach ($xpath->query($xpath_query) as $element) {
      /** @var \DOMElement $element */
      try {
        // Get the href value of the <a> element.
        $match[] = $element->getAttribute('href');
      } catch (\Exception $e) {
        // Do nothing.
      }
    }
    return $match;
  }

  /**
   * Helper function that converts an uri to an absolute uri.
   *
   * @param string $uri
   *   Input uri that is either an absolute url or a Drupal internal url.
   *
   * @return string
   *   Absolute uri.
   */
  protected function convertUriToAbsoluteUri(string $uri) {
    // Absolute uri.
    if (str_starts_with($uri, 'http')) {
      return $uri;
    }
    // Relative uri.
    if (str_starts_with($uri, '/') || str_starts_with($uri, '#') || str_starts_with($uri, '?')) {
      return Url::fromUserInput($uri, ['absolute' => TRUE])->toString();
    }
    // Schema uri.
    if (str_starts_with($uri, 'internal:') || str_starts_with($uri, 'base:') || str_starts_with($uri, 'entity:') || str_starts_with($uri, 'route:')) {
      return Url::fromUri($uri, ['absolute' => TRUE])->toString();
    }
    // Special case for <front>.
    if ($uri === '<front>') {
      return Url::fromUri('internal:/', ['absolute' => TRUE])->toString();
    }
    if ($uri === '<none>') {
      return Url::fromUri('internal:', ['absolute' => TRUE])->toString();
    }
    return $uri;
  }

}
