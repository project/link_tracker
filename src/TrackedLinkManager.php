<?php

namespace Drupal\link_tracker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_track\EntityTrackStorageInterface;

/**
 * Helper service to manage the storage for tracked_link entities.
 */
class TrackedLinkManager implements EntityTrackStorageInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * Register a record for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The tracked entity.
   * @param string $field_name
   *   The name of the tracked field.
   * @param string $uri
   *   The URI that is tracked.
   */
  public function register($entity, $field_name, $uri) {
    $storage = $this->entityTypeManager->getStorage('tracked_link');

    $link_tracker = $storage->create([
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'entity_langcode' => $entity->language()->getId(),
      'field_name' => $field_name,
      'link_uri' => $uri,
      'link_status_code' => NULL,
    ]);
    $link_tracker->save();
  }

  /**
   * Delete all records for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The deleted entity.
   */
  public function deleteByEntity($entity) {
    $storage = $this->entityTypeManager->getStorage('tracked_link');
    $records = $storage->loadByProperties([
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
    ]);
    $storage->delete($records);
  }

  /**
   * Delete all records for a given entity translation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The deleted entity translation.
   */
  public function deleteByTranslation($entity) {
    $storage = $this->entityTypeManager->getStorage('tracked_link');
    $records = $storage->loadByProperties([
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'entity_langcode' => $entity->language()->getId(),
    ]);
    $storage->delete($records);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTrackingInformation() {
    $storage = $this->entityTypeManager->getStorage('tracked_link');
    $issues = $storage->loadMultiple();
    $storage->delete($issues);
  }

  /**
   * Load all records for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The tracked entity.
   *
   * @return \Drupal\link_tracker\Entity\TrackedLink[]
   *   The tracking records.
   */
  public function loadByEntity($entity) {
    $storage = $this->entityTypeManager->getStorage('tracked_link');
    /** @var \Drupal\link_tracker\Entity\TrackedLink[] $records */
    $records = $storage->loadByProperties([
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
    ]);
    return $records;
  }

  /**
   * Load all records for a given entity and field name.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The tracked entity.
   * @param string $field_name
   *   The name of the field.
   *
   * @return \Drupal\link_tracker\Entity\TrackedLink[]
   *   The tracking records.
   */
  public function loadByEntityAndField($entity, $field_name) {
    $storage = $this->entityTypeManager->getStorage('tracked_link');
    /** @var \Drupal\link_tracker\Entity\TrackedLink[] $records */
    $records = $storage->loadByProperties([
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'field_name' => $field_name,
    ]);
    return $records;
  }

  /**
   * Load records for a given entity, field name and uri.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The tracked entity.
   * @param string $field_name
   *   The name of the field.
   * @param string $link_uri
   *   The link uri.
   *
   * @return \Drupal\link_tracker\Entity\TrackedLink[]
   *   The tracking records.
   */
  public function loadByEntityAndFieldAndUri($entity, $field_name, $link_uri) {
    $storage = $this->entityTypeManager->getStorage('tracked_link');
    /** @var \Drupal\link_tracker\Entity\TrackedLink[] $records */
    $records = $storage->loadByProperties([
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'field_name' => $field_name,
      'link_uri' => $link_uri,
    ]);
    return $records;
  }

  /**
   * Load records sorted by last_checked_date.
   *
   * @param int $limit
   *   Limit max records.
   * @param int $last_checked_threshold
   *   Cutoff timestamp. Use this to get records that were checked x time or
   *   longer in the past.
   *
   * @return \Drupal\link_tracker\Entity\TrackedLink[]
   *   The tracking records.
   */
  public function loadByLastChecked($limit = 50, $last_checked_threshold = NULL) {
    $storage = $this->entityTypeManager->getStorage('tracked_link');

    // First try to return items that have not been processed.
    $query = $storage->getQuery()
      ->notExists('link_last_checked')
      ->range(0, $limit);
    $tracked_link_ids = $query->execute();

    // Otherwise return processed items before last_checked_upper.
    if (!count($tracked_link_ids)) {
      $last_checked_threshold = $last_checked_threshold ?? time();
      $query = $storage->getQuery()
        ->condition('link_last_checked', $last_checked_threshold, '<')
        ->sort('link_last_checked')
        ->range(0, $limit);
      $tracked_link_ids = $query->execute();
    }

    /** @var \Drupal\link_tracker\Entity\TrackedLink[] $records */
    $records = $storage->loadMultiple($tracked_link_ids);
    return $records;
  }

}
