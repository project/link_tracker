<?php


namespace Drupal\link_tracker;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;
use GuzzleHttp\Client;

/**
 * Class StatusCodeChecker
 *
 * @package Drupal\link_tracker
 */
class StatusCodeChecker {

  use FieldExtractorTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   *   The link tracker storage.
   *
   * @var \Drupal\link_tracker\TrackedLinkManager
   */
  protected $trackingStorage;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a new TrackTestStorage object.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   * @param \Drupal\link_tracker\TrackedLinkManager $tracked_link_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   */
  public function __construct(Client $httpClient, TrackedLinkManager $tracked_link_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $httpClient;
    $this->trackingStorage = $tracked_link_manager;
    $this->logger = $logger_factory->get('link_tracker');
  }

  /**
   * Check status code
   *
   * @param int $limit
   *   Maximum number of links to check.
   * @param int $last_checked_threshold
   *   Cutoff timestamp. Use this to get records that were checked x time or
   *   longer in the past.
   */
  public function checkStatusCodes($limit = 50, $last_checked_threshold = NULL) {
    $tracked_links = $this->trackingStorage->loadByLastChecked($limit, $last_checked_threshold);

    foreach ($tracked_links as $link) {
      $uri = $link->get('link_uri')->value;
      try {
        $absolute_uri = $this->convertUriToAbsoluteUri($uri);
        $response = $this->httpClient->head($absolute_uri, [
          'http_errors' => FALSE,
          'allow_redirects' => TRUE,
          'timeout' => Settings::get('link_tracker_cron_status_code_checker_timeout', 5),
        ]);
      } catch (\Exception $e) {
        $this->logger->error(t('Error: Failed checking uri: @uri with error: "@message"', ['@uri' => $uri, '@message' => $e->getMessage()]));
        // Write a status-code to prevent that the queue keeps prioritizing
        // faulty tracked-links.
        $link->set('link_status_code', 500)
          ->set('link_status_error_message', $e->getMessage())
          ->set('link_last_checked', time())
          ->save();
        continue;
      }

      $link->set('link_status_code', $response->getStatusCode())
        ->set('link_last_checked', time())
        ->save();
    }
  }

}
