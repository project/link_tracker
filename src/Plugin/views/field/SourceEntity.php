<?php

namespace Drupal\link_tracker\Plugin\views\field;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to that renders a link to the source entity.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("link_tracker_source_entity")
 */
class SourceEntity extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $tracked_link = $values->_entity;
    $entity_type = $tracked_link->get('entity_type')->value;
    $entity_id = $tracked_link->get('entity_id')->target_id;

    $title = 'View source';
    $url = Url::fromRoute('entity_usage.usage_list', [
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    ]);
    return Link::fromTextAndUrl($title, $url)->toString();
  }

}
