<?php

namespace Drupal\link_tracker\Plugin\EntityTrack\Track;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableRevisionableInterface;
use Drupal\entity_track\EntityTrackBase;
use Drupal\link_tracker\FieldExtractorTrait;
use Drupal\link_tracker\TrackedLinkManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin that tracks LinkFields.
 *
 * @EntityTrack(
 *   id = "link_tracker_link_field",
 *   label = @Translation("Link field"),
 *   field_types = {"link"},
 * )
 */
class LinkField extends EntityTrackBase {

  use FieldExtractorTrait;

  /**
   *   The link tracker storage.
   *
   * @var \Drupal\link_tracker\TrackedLinkManager
   */
  protected $trackingStorage;

  /**
   * Plugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The EntityFieldManager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\link_tracker\TrackedLinkManager $tracked_link_manager
   *   The link tracker storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory, TrackedLinkManager $tracked_link_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $config_factory);
    $this->trackingStorage = $tracked_link_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('link_tracker.tracked_link_manager')
    );
  }

  /**
   * @inheritDoc
   */
  public function trackOnEntityCreation(EntityInterface $entity) {
    $extracted_field_values = $this->extractFieldValues($entity);
    $extracted_links = $this->extractLinksFromFieldValues($extracted_field_values);

    // On creation register tracked links for all uri's found.
    foreach ($extracted_links as $field_name => $extracted_field_links) {
      foreach ($extracted_field_links as $uri) {
        if (!empty($this->trackingStorage->loadByEntityAndFieldAndUri($entity, $field_name, $uri))) {
          continue;
        }
        $this->trackingStorage->register($entity, $field_name, $uri);
      }
    }
  }

  /**
   * @inheritDoc
   */
  public function trackOnEntityUpdate(EntityInterface $entity) {
    $extracted_field_values = $this->extractFieldValues($entity);
    $new_extracted_links = $extracted_links = $this->extractLinksFromFieldValues($extracted_field_values);

    // Loop through existing tracked links and compare it with extracted links.
    foreach ($extracted_links as $field_name => $items) {
      foreach ($this->trackingStorage->loadByEntityAndField($entity, $field_name) as $registered_link) {
        // Do not recreate a link that is unchanged in the updated entity.
        foreach (array_keys($items, $registered_link->link_uri->value, TRUE) as $key) {
          unset($new_extracted_links[$field_name][$key]);
          continue 2;
        }
        // Delete stale tracked links that are removed from the updated entity.
        $registered_link->delete();
      }
    }

    // Create new untracked links.
    foreach ($new_extracted_links as $field_name => $items) {
      foreach ($items as $uri) {
        $this->trackingStorage->register($entity, $field_name, $uri);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function trackOnEntityDelete(EntityInterface $entity, $type) {
    // When an entity is being deleted the logic is much simpler and we just
    // delete the records that affect this entity both as target and source.
    switch ($type) {
      case 'translation':
        $this->trackingStorage->deleteByTranslation($entity);
        break;

      case 'revision':
        if ($entity instanceof TranslatableRevisionableInterface && $entity->isDefaultRevision()) {
          $this->trackingStorage->deleteByEntity($entity);
        }
        break;

      default:
        $this->trackingStorage->deleteByEntity($entity);
        break;
    }
  }

  /**
   * Extract links from link-field values.
   *
   * @param array $field_values
   *   Array of field values.
   *
   * @return array
   *   Flatten array of URIs.
   */
  private function extractLinksFromFieldValues($field_values) {
    $links = [];
    foreach ($field_values as $field_name => $items) {
      foreach ($items as $field_value) {
        $links[$field_name][] = $field_value['uri'];
        $links[$field_name] = array_unique($links[$field_name]);
      }
    }
    return $links;
  }

}
