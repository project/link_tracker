<?php

namespace Drupal\link_tracker\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the entity to track issues in nodes.
 *
 * @ContentEntityType(
 *   id = "tracked_link",
 *   label = @Translation("Tracked link"),
 *   base_table = "tracked_link",
 *   translatable = FALSE,
 *   revisionable = FALSE,
 *   handlers = {
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   admin_permission = "administer tracked links",
 *   links = {
 *     "collection" = "/admin/reports/link-tracker",
 *   }
 * )
 */
class TrackedLink extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('The type of the tracked entity.'));

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The ID of the tracked entity.'));

    $fields['entity_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The langcode of the tracked entity.'));

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Field'))
      ->setDescription(t('The field name where the tracking information was found.'));

    $fields['link_uri'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('URI'))
      ->setDescription(t('The link URI.'));

    $fields['link_status_code'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Status code'))
      ->setDescription(t('The link HTML status code.'))
      ->setDefaultValue(NULL);

    $fields['link_status_error_message'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Status error message'))
      ->setDescription(t('The link status error message.'))
      ->setDefaultValue(NULL);

    $fields['link_last_checked'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last checked'))
      ->setDescription(t('When status code was last checked.'));

    return $fields;
  }

}
