<?php

/**
 * Implements hook_views_data_alter().
 */
function link_tracker_views_data_alter(array &$data) {
  $data['tracked_link']['source_entity'] = [
    'title' => t('Source entity'),
    'group' => t('Entity Usage'),
    'field' => [
      'title' => t('Source entity'),
      'help' => t('Entity where tracked link can be changed.'),
      'id' => 'link_tracker_source_entity',
    ],
  ];
}
